import { combineReducers } from "redux"

const songsReducer = () => {
    return [
        {title: 'Shape of you' , duration: '3:40'},
        {title: 'eiei' , duration: '2:40'},
        {title: 'Enemy' , duration: '4:40'},
        {title: 'Sunday morning' , duration: '4:20'},
    ]
}

const selectedSongReducer = (selectedSong=null, action) => {
    if(action.type === 'SONG_SELECTE') {
        return action.payload
    }
    return selectedSong
}

function counterReducer(state = {step:10}, action) {
    switch (action.type) {    
      case 'INCR':
        const {step} = state
        return {...state,step: step + action.payload.step}
      case 'SONG_SELECTE1':
        return state + 100
      default:
        return state
    }
  }

export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer,
    counter: counterReducer,
})