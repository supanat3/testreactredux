export const selectSong = song => {
     return {
         type: 'SONG_SELECTE',
         payload: song
     }
}

export const incr = (number = 1) => {
    return {
        type: 'INCR',
        payload: {step: number}
    }
}
