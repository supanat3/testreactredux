import React from "react";
import { connect } from "react-redux";
import { selectSong, incr } from "../actions";
const SongList = (props) => {
  const renderListSong = () => {
    return props.songs.map((song, index) => {
      return (
        <div className="item" key={song.title}>
          <div className="right floated content">
            <button
              className="ui button primary"
              onClick={() => props.selectSong(song)}
            >
              Select
            </button>
          </div>
          <div className="content">{song.title}</div>
        </div>
      );
    });
  };
  return (
    <div className="ui divided list">
      {renderListSong()}
      <div>
        counterReducer :{props.count.step}
        <button
          className="ui button primary"
          onClick={() => props.incr(5)}
        >
         Action
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return { songs: state.songs, count: state.counter };
};

export default connect(mapStateToProps, {
  selectSong,
  incr,
})(SongList);
